//
//  ViewController.swift
//  Swift5Timer1
//
//  Created by hayashi kenji on 2019/09/23.
//  Copyright © 2019 hayashi kenji. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var startButton: UIButton!
    @IBOutlet var stopButton: UIButton!
    var timer = Timer()
    var count = Int()
    var imageArray = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        count = 0
        //stopButtonを押せなくする
        stopButton.isEnabled = false
        
        for i in 0..<5{
            print(i)
            let image = UIImage(named: "\(i)")
            imageArray.append(image!)
            
        }
        
        imageView.image = UIImage(named: "0")
        
    }
    
    func startTimer(){
        //タイマーを回す　0.2秒ごとにメソッドを呼ぶ
        timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(timerUpdate), userInfo: nil, repeats: true)
    }
    //0.2秒ごとに呼ばれる
    @objc func timerUpdate(){
        count += 1
        if count > 4{
            count = 0
        } else {
        imageView.image = imageArray[count]
        }
        
        
    }

    @IBAction func start(_ sender: Any) {
        //imageViewのimageに画像を反映する
        
        //startButtonは押せなくする
        startButton.isEnabled = false
        stopButton.isEnabled = true
        startTimer()
        
        
    }
    
    
    
    @IBAction func stop(_ sender: Any) {
        
        //imageViewのimageに反映されている画像を停止させる
        
        //startButtonを押せるようにする
        startButton.isEnabled = true
        stopButton.isEnabled = false
        
        //タイマーを止める
        timer.invalidate()
        
        
    }
    
    
    
}

